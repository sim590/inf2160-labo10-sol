## 1 - Bases de connaissance

> Représentez dans une base de connaissance le graphe suivant, tiré de la page
> Wikipedia portant sur l'algorithme de Dijkstra:
>
> ![](https://upload.wikimedia.org/wikipedia/commons/5/57/Dijkstra_Animation.gif)
>
> Utilisez les structures suivantes:
>
> - `edge(1, 2, 7)` pour indiquer une arête qui lie les sommets `1` et `2` avec
>   un poids de `7`.
> - `arc(From, To, Length)` pour identifier un arc allant de `From` vers `To`
>   avec un poids de `Length`. Notez qu'une arête (*edge*) est non orientée,
>   alors qu'un arc l'est.

La solution poru les arrêtes est la suivante:

```prolog
edge(1, 2, 7).
edge(1, 3, 9).
edge(1, 6, 14).
edge(2, 3, 10).
edge(2, 4, 15).
edge(3, 4, 11).
edge(3, 6, 2).
edge(4, 5, 6).
edge(5, 6, 9).
```

Pour les arcs, on peut donc écrire la chose suivante:

```prolog
arc(From, To, Length) :- edge(From, To, Length).
arc(From, To, Length) :- edge(To, From, Length).
```

## 2 - Minimum d'une liste de structures

> Nous aimerions pouvoir trier des expressions complexes. Par exemple,
> considérons la liste `[0-[3,2], 4-[1]]`. Je vous rappelle qu'en Prolog, la
> soustraction n'est pas différente d'un autre foncteur tant qu'elle n'est pas
> interprétée comme telle.
>
> Que se passe-t-il si vous entrez l'instruction suivante dans l'interpréteur
> Prolog:
> ```prolog
> min_list([0-[3,2], 4-[1]], M).
> ```
>
> Pour rectifier la situation, nous allons construire notre propre prédicat
> `minimum/2` qui permet de calculer le minimum d'une liste même quand
> l'expression est complexe.
>
> Dans un premire temps, consultez la documentation du prédicat `keysort/2`:
> http://www.swi-prolog.org/pldoc/man?predicate=keysort/2
>
> Ensuite, à l'aide de ce prédicat, proposez l'implémentation d'un prédicat
> ```prolog
> minimum(L, M)
> ```
> qui est vérifié si `M` est le minimum de la liste `L`. Testez ensuite votre
> prédicat:
> ```prolog
> minimum([0-[3,2], 4-[1]], M).
> ```

Voir le code du fichier [`dijkstra.pl`][dijkstra]. La solution s'y trouve.

[dijkstra]: https://gitlab.com/ablondin/inf2160-exercices/blob/master/labo10/dijkstra.pl

<!-- vim: set ts=4 sw=4 tw=80 et :-->

